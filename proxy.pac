function FindProxyForURL(url, host)
{
  //Access images directly
  if (host == "image.xxxxxx.co.jp") return "DIRECT";

  //For everything else use stg proxy
  return "PROXY [プロキシサーバー]";
}
