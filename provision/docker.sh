#!/bin/sh

# Dockerのインストール
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce docker-ce-cli containerd.io

# 自動起動設定
systemctl enable docker

# Docekrの起動
systemctl start docker

# Docker composeのインストール
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# ビルド
cd /vagrant
docker-compose build

