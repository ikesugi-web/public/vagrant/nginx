#!/bin/sh

cp /vagrant/.ssh/* ~/.ssh/
#ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
#ssh -Tv git@gitlab.com

# Gitリポジトリからアプリのソースをcloneする
# frontend
cd /vagrant/docker
#git clone https://$1:$2@gitのリポジトリ.git app
git clone git@gitlab.com:ikesugi-web/public/vue/nuxt.git app
#git clone https://gitlab.com/ikesugi-web/public/vue/nuxt.git app

# backend
cd /vagrant/docker
#git clone https://$1:$2@gitリポジトリ.git api
git clone git@gitlab.com:ikesugi-web/public/php/laravel6.git api
#git clone https://gitlab.com/ikesugi-web/public/php/laravel6.git api
