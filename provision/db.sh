#!/bin/sh

# MySQL
#yum remove mariadb-libs
#rm -rf /var/lib/mysql/
rpm -Uvh https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
#yum -y install https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
yum install --enablerepo=mysql80-community mysql-community-server
mysqld --initialize-insecure --user=root
systemctl enable mysqld
systemctl restart mysqld
