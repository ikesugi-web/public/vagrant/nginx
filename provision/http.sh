#!/bin/sh

yum -y install httpd
systemctl enable httpd


httpdconf="
<VirtualHost *:80>
    ServerName nama.local
    DocumentRoot "/var/www/app"
    <Directory "/var/www/app">
    Options FollowSymLinks
    AllowOverride all
    Require all granted
    </Directory>
</VirtualHost>
"
if [ ! -e /etc/httpd/conf.d/laravel_httpd.conf ]; then
    echo "$httpdconf" > "/etc/httpd/conf.d/laravel_httpd.conf"
fi

